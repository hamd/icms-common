-- IMPORTANT: this is not (yet) an autonomous script, please execute the statements manually,
-- following provided instructions

-- 1.0 Create a service account with sufficient rights to perform all the remaining procedures
CREATE ROLE serve NOSUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'protect';
-- 1.1 Create a wireframe DB for all the iCMS stuff
CREATE DATABASE icms owner serve;
-- 1.2 Like 1.1, but for testing
CREATE DATABASE icms_test owner serve;


-- 2.0 Login as service account (psql -U serve -d icms)
-- 2.1 Create roles needed for individual schemas in icms database (service account needs to be a member of these)
-- (DOC: To create a schema owned by another role, you must be a direct or indirect member of that role, or be a superuser.)
CREATE ROLE toolkit login encrypted password 'icms' ROLE serve;
CREATE ROLE icms login encrypted password 'icms' ROLE serve;
CREATE ROLE epr login encrypted password 'icms' ROLE serve;
CREATE ROLE notes login encrypted password 'icms' ROLE serve;
CREATE ROLE cadi login encrypted password 'icms' ROLE serve;
-- 2.2 Create needed schemas and set their ownership correspondingly
\c icms;
CREATE SCHEMA toolkit AUTHORIZATION toolkit;
CREATE SCHEMA epr AUTHORIZATION epr;
CREATE SCHEMA notes AUTHORIZATION notes;
CREATE SCHEMA cadi AUTHORIZATION cadi;
-- 2.4.0 Create a generic icms_reader role
CREATE ROLE icms_reader;
-- 2.4.1 That can connect to icms database
GRANT CONNECT ON DATABASE icms to icms_reader;
-- 2.4.2 That can use all the schemas
GRANT USAGE ON SCHEMA epr to icms_reader;
GRANT USAGE ON SCHEMA toolkit to icms_reader;
GRANT USAGE ON SCHEMA notes to icms_reader;
GRANT USAGE ON SCHEMA cadi to icms_reader;
GRANT USAGE ON SCHEMA public to icms_reader;
-- 2.4.3 That can select from all tables in all schemas
GRANT SELECT ON ALL TABLES IN SCHEMA epr to icms_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA toolkit to icms_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA notes to icms_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA cadi to icms_reader;
GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to icms_reader;
GRANT CREATE ON SCHEMA public to icms_reader;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA public to icms_reader;
-- 2.4.4 And that is a group role for schema owners
GRANT icms_reader to epr, toolkit, icms, cadi, notes;
-- 2.4.5 A sad hack: the icms role needs to be able to create views in the epr schema (at least as long as the migration scripts are distributed the way they are now, April 2020)
GRANT epr to icms;



-- 3.0 Login as service account to the test database (psql -U serve -d icms_test)
-- 3.1 Mirror point 2 for icms_test database...
CREATE ROLE toolkit_test login encrypted password 'icms_test' ROLE serve;
CREATE ROLE epr_test login encrypted password 'icms_test' ROLE serve;
CREATE ROLE icms_test login encrypted password 'icms_test' ROLE serve;
CREATE ROLE notes_test login encrypted password 'icms_test' ROLE serve;
-- ? which symmetry to follow: same schema names as in icms or schema_name=user_name so that path lookups work the same?
\c icms_test;
CREATE SCHEMA toolkit AUTHORIZATION toolkit_test;
CREATE SCHEMA epr AUTHORIZATION epr_test;
CREATE SCHEMA notes AUTHORIZATION notes_test;

alter role toolkit_test set search_path = toolkit, public;
alter role epr_test set search_path = epr, public;
alter role notes_test set search_path = notes, public;

CREATE ROLE icms_test_reader;
GRANT CONNECT ON DATABASE icms_test to icms_test_reader;
GRANT USAGE ON SCHEMA epr to icms_test_reader;
GRANT USAGE ON SCHEMA toolkit to icms_test_reader;
GRANT USAGE ON SCHEMA notes to icms_test_reader;
GRANT USAGE ON SCHEMA public to icms_test_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA epr to icms_test_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA toolkit to icms_test_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA notes to icms_test_reader;
GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to icms_test_reader;
GRANT CREATE ON SCHEMA public to icms_reader;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA public to icms_test_reader;
GRANT icms_test_reader to epr_test, toolkit_test, icms_test, notes_test;

-- and the infamous grant (explained above, in dev DB section)
GRANT epr_test to icms_test;

-- use this to store the dump
-- PGPASSWORD=protect pg_dump -U serve -d icms --schema epr --no-owner --format custom --compress 9 -f epr.dmp
-- then this one to reload it
-- PGPASSWORD=protect pg_restore -U serve -d icms_test --clean epr.dmp


-- the following is needed for the tests to execute (SQLAlchemy creates mysql enums in psql schemas
-- and then there are access violation issues whenever some other role tries to drop them)
drop schema epr cascade;
drop schema public cascade;
drop schema toolkit cascade;

create schema epr authorization icms_test_reader;
create schema public authorization icms_test_reader;
create schema toolkit authorization icms_test_reader;

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
COMMENT ON SCHEMA public IS 'standard public schema';