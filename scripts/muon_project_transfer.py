from scripts.prescript_setup import db, config, ConfigProxy
from icms_orm.cmspeople import Person, MemberStatusValues as MSV
from icmsutils.prehistory import PersonAndPrehistoryWriter
import sqlalchemy as sa
import logging
import sys


class MuonProjectTransferrer(object):
    def __init__(self, dry_run):
        self._just_try = dry_run

    def execute(self):
        actor = db.session.query(Person).filter(Person.cmsId == ConfigProxy.get_updater_cms_id_for_scripts()).one()
        people = db.session.query(Person).filter(sa.and_(
            Person.project.like('MU%'),
            Person.project.notlike('Muon'),
            sa.or_(Person.status == MSV.PERSON_STATUS_CMS, Person.status == MSV.PERSON_STATUS_CMSEMERITUS)
        )).all()
        for p in people:
            assert isinstance(p, Person)
            logging.info(
                'To be transferred to Muon: CMS ID {0}, CMS status {1} and project {2}'.format(p.cmsId, p.status,
                                                                                               p.project))
            pw = PersonAndPrehistoryWriter(db.session, p, actor)
            pw.set_new_value(Person.project, 'Muon')
            pw.apply_changes(do_commit=False)
        if self._just_try:
            logging.info('Only testing -> rolling back the transaction. Starred: {0} as actor.'.format(actor.cmsId))
            db.session.rollback()
        else:
            db.session.commit()
            logging.info('DB session committed, {0} changes performed. Starred: {1} as actor.'.format(len(people), actor.cmsId))


if __name__ == '__main__':
    dry_run = any([x in map(str.lower, sys.argv) for x in ('dry', 'dryrun', 'dry_run', 'dry-run')])
    logging.info('About to launch with dry_run={0}'.format(dry_run))
    mpt = MuonProjectTransferrer(dry_run)
    mpt.execute()
