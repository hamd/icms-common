from typing import List, Optional
from icmsutils.dbutils import join_against_triggers_output, get_ref_ids_of_deleted_records
from datetime import date
from scripts.prescript_setup import config, ConfigProxy
from sqlalchemy.orm import Query
import abc


class AbstractBroker(abc.ABC):
    @abc.abstractmethod
    def relay(self, query: Query) -> List:
        pass

    @classmethod
    def is_incremental(cls) -> bool:
        return False

    @classmethod
    def force_enabled(cls):
        """
        Overrides current config settings to enable this broker class (useful mainly for testing)
        """
        ConfigProxy.set_sync_broker_class_name(cls.__name__)

    @classmethod
    def get_subclass_by_name(cls, name) -> Optional[type]:
        def _get_subclasses_list(cls):
            _list = [cls]
            for _cls in cls.__subclasses__():
                _list += _get_subclasses_list(_cls)
            return _list
        return {_cls.__name__: _cls for _cls in _get_subclasses_list(cls)}.get(name, None)


class DefaultBroker(AbstractBroker):
    """
    Base class for grabbing rows from the old database
    """
    def relay(self, query: Query) -> List:
        return query.all()


class ChangeLogAwareBroker(AbstractBroker):
    """
    A more sophisticated implementation that takes into account the trigger data
    """

    def __init__(self, since_when=None):
        # if no reasonable date has been provided, all the records will be returned for cross-checking
        self._since_when = since_when or date(1999, 1, 1)

    def relay(self, query: Query):
        return join_against_triggers_output(query, since_when=self._since_when).all()

    def set_since_when(self, since_when: date):
        self._since_when = since_when or date(1999, 1, 1)

    def get_ref_ids_of_deleted_records(self, entity_class):
        return get_ref_ids_of_deleted_records(entity_class=entity_class, since_when=self._since_when)

    @property
    def since_when(self):
        return self._since_when

    @classmethod
    def is_incremental(cls):
        return True
