import logging

import sqlalchemy as sa
from icms_orm import common
from icms_orm.cmspeople import Institute as SrcInst
from icms_orm.cmspeople import Person as SrcPerson, User as SrcUser, MemberActivity as SrcActivity
from icms_orm.cmspeople import PersonHistory as SrcHistory
from icms_orm.common import InstituteLeader, Affiliation
from icms_orm.common import Person as DstPerson, PersonStatus as DstPersonStatus
from icms_orm.common import PrehistoryTimeline
from sqlalchemy import case

from icmsutils.prehistory import SupersedersResolver
from icmsutils.prehistory.prehistory_model import PrehistoryModel
from scripts.mockables import today
from scripts.prescript_setup import db
from scripts.sync.agents import CountriesSyncAgent
from scripts.sync.agents.sync_agents_common import BaseSyncAgent


class PrehistorySyncAgent(BaseSyncAgent):

    @staticmethod
    def are_sets_of_timelines_identical(set_a, set_b):
        empty_count = sum(not x for x in [set_a, set_b])
        if empty_count == 2:
            logging.debug('TL sets identical (both empty)')
            return True
        elif empty_count == 1:
            logging.debug('Only one TL set is empty (so they differ)')
            return False

        if len(set_a) != len(set_b):
            logging.debug('Different length of time-line-sets')
            return False
        else:
            # probably same length, need element-by-element comparison...
            for el_a, el_b in zip(set_a, set_b):
                for field in [PrehistoryTimeline.start_date, PrehistoryTimeline.end_date, PrehistoryTimeline.cms_id,
                              PrehistoryTimeline.status_cms, PrehistoryTimeline.inst_code,
                              PrehistoryTimeline.activity_cms]:
                    # set of unique elements should have a length one if these elements are identical
                    if len({el.get(field) for el in [el_a, el_b]}) != 1:
                        logging.debug('Time-lines seem to differ wrt value of %s' % field.key)
                        return False
        logging.debug('Time-line sets seem to be identical')
        return True

    def sync(self):
        # get the sources and sort them by CMS IDs
        sources = {x.get(SrcHistory.cmsId) : x for x in self._broker.relay(db.session.query(SrcHistory))}
        people = {p.get(SrcPerson.cmsId): p for p in SrcPerson.session().query(SrcPerson).filter(SrcPerson.cmsId.in_(sources.keys())).all()}
        pre_synced = PrehistoryTimeline.session().query(PrehistoryTimeline).filter(PrehistoryTimeline.cms_id.in_(sources.keys())).order_by(PrehistoryTimeline.start_date, PrehistoryTimeline.id).all()
        sorted_pre_synced = dict()
        logging.debug('Found %d pre-synced time lines for %d relevant CMS ids' % (len(pre_synced), len(sources)))
        for h in pre_synced:
            sorted_pre_synced[h.cms_id] = sorted_pre_synced.get(h.cms_id, [])
            sorted_pre_synced[h.cms_id].append(h)
        pre_synced = sorted_pre_synced

        for cms_id in sources.keys():
            pm = PrehistoryModel(cms_id=cms_id, person=people.get(cms_id), history=sources.get(cms_id))
            possible_replacement_time_lines = pm.get_prehistory_time_lines()
            existing_time_lines = pre_synced.get(cms_id)
            if PrehistorySyncAgent.are_sets_of_timelines_identical(existing_time_lines, possible_replacement_time_lines):
                logging.debug('Prehistory time lines sets are already up to date for CMS ID %d' % cms_id)
                continue
            else:
                logging.debug('Going to update prehistory time lines for CMS ID %d' % cms_id);
                # ignore output, but execute the list comprehension
                _ = [PrehistoryTimeline.session().delete(tl) for tl in existing_time_lines or []]
                _ = [PrehistoryTimeline.session().add(tl) for tl in possible_replacement_time_lines or []]
        PrehistoryTimeline.session().commit()