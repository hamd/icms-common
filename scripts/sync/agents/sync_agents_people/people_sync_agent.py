import logging

import sqlalchemy as sa
from icms_orm import common
from icms_orm.cmspeople import Institute as SrcInst
from icms_orm.cmspeople import Person as SrcPerson, User as SrcUser, MemberActivity as SrcActivity
from icms_orm.cmspeople import PersonHistory as SrcHistory
from icms_orm.common import InstituteLeader, Affiliation
from icms_orm.common import Person as DstPerson, PersonStatus as DstPersonStatus
from icms_orm.common import PrehistoryTimeline
from sqlalchemy import case
from sqlalchemy.sql.schema import Column

from icmsutils.prehistory import SupersedersResolver
from icmsutils.prehistory.prehistory_model import PrehistoryModel
from scripts.mockables import today
from scripts.prescript_setup import db
from scripts.sync.agents import CountriesSyncAgent
from scripts.sync.agents.sync_agents_common import BaseSyncAgent
from datetime import date, datetime


class PeopleSyncAgent(BaseSyncAgent):
    def create_a_new_status(self, cms_id, status, is_author, activity, author_block, epr_suspension):
        logging.info('Creating a new status "%s" for CMS ID %d holder.' % (status, cms_id))
        status = status and status.strip() or None
        return DstPersonStatus.from_ia_dict({
            DstPersonStatus.cms_id: cms_id, DstPersonStatus.status: status, DstPersonStatus.start_date: today(),
            DstPersonStatus.activity: activity, DstPersonStatus.is_author: is_author,
            DstPersonStatus.epr_suspension: epr_suspension, DstPersonStatus.author_block: author_block
        })

    def sync(self):
        ssn = db.session
        existing_data = ssn.query(DstPerson, DstPersonStatus).outerjoin(DstPersonStatus, sa.and_(
            DstPerson.cms_id == DstPersonStatus.cms_id, DstPersonStatus.end_date == None)).all()

        existing = {getattr(r[0], DstPerson.cms_id.key): r[0] for r in existing_data}
        statuses = {getattr(r[1], DstPersonStatus.cms_id.key): r[1] for r in existing_data if r[1]}
        logging.info('Found %d existing entries' % len(existing))
        # unless the mailWhere column says institute, prioritize the emailCern!

        queried = [SrcPerson.cmsId, SrcPerson.hrId, SrcPerson.loginId, SrcPerson.lastName, SrcPerson.firstName,
                   sa.func.coalesce(
                       case(whens=[(SrcUser.mailWhere.ilike('institute'), None)], else_=SrcUser.emailCern),
                       SrcUser.email1, SrcUser.email2, SrcUser.emailCern
                   ), SrcUser.sex, SrcPerson.nationality, SrcPerson.birthDate, 
                   SrcPerson.status, SrcPerson.instCode, SrcPerson.isAuthor, SrcActivity.name,
                   SrcPerson.isAuthorBlock, SrcPerson.isAuthorSuspended]

        query = ssn.query(*queried).join(SrcUser, SrcPerson.cmsId == SrcUser.cmsId).\
            join(SrcActivity, SrcActivity.id == SrcPerson.activityId, isouter=True)

        sources = {r[0]: r for r in self._broker.relay(query)}
        logging.info('Found %d source entries' % len(sources))

        inserted = [DstPerson.cms_id, DstPerson.hr_id, DstPerson.login, DstPerson.last_name, DstPerson.first_name,
                    DstPerson.email, DstPerson.gender, DstPerson.nationality, DstPerson.date_of_birth]

        for cms_id, data in sources.items():
            if cms_id not in existing:
                attr_dict = {}
                for i, col in enumerate(inserted):
                    attr_dict[col] = self.__translate(col, data[i])
                ssn.add(DstPerson.from_ia_dict(attr_dict))
                ssn.flush()
                logging.info('Adding a new user with CMS ID %d' % cms_id)
            else:
                counterpart = existing[cms_id]
                changed = False
                for i, col in enumerate(inserted):
                    translation = self.__translate(col, data[i])
                    if translation != getattr(counterpart, col.key):
                        setattr(counterpart, col.key, translation)
                        changed = True
                if changed:
                    logging.info('Entry for CMS ID %d has changed! Updating...' % cms_id)
                    ssn.add(counterpart)
                    ssn.flush()

            # sync the status
            fetched_status = data[queried.index(SrcPerson.status)] or None
            fetched_is_author = data[queried.index(SrcPerson.isAuthor)]
            fetched_activity = data[queried.index(SrcActivity.name)]
            fetched_suspended = data[queried.index(SrcPerson.isAuthorSuspended)]
            fetched_blocked = data[queried.index(SrcPerson.isAuthorBlock)]
            if cms_id not in statuses:
                ssn.add(self.create_a_new_status(cms_id, fetched_status, fetched_is_author, fetched_activity,
                                                 fetched_blocked, fetched_suspended))
            else:
                current_status_entry = statuses.get(cms_id)
                if any([current_status_entry.get(_col) != _fetched for _fetched, _col in [
                    (fetched_activity, DstPersonStatus.activity), (fetched_blocked, DstPersonStatus.author_block),
                    (fetched_is_author, DstPersonStatus.is_author), (fetched_status, DstPersonStatus.status),
                    (fetched_suspended, DstPersonStatus.epr_suspension)
                ]]):
                    current_status_entry.set(DstPersonStatus.end_date, today())

                    ssn.add(statuses.get(cms_id))
                    ssn.add(self.create_a_new_status(cms_id, fetched_status, fetched_is_author, fetched_activity,
                                                     fetched_blocked, fetched_suspended))
        ssn.commit()

    def __translate(self, dst_column_type: Column, src_value):
        if dst_column_type == DstPerson.gender:
            return {'F': common.GenderValues.FEMALE, 'M': common.GenderValues.MALE}.get(src_value and src_value.upper(), None)
        if dst_column_type == DstPerson.date_of_birth:
            if isinstance(src_value, date) and src_value.year >= 1800:
                return src_value
            return None
        elif dst_column_type == DstPerson.nationality:
            # trying to express the old DB's nationality info through a reference to new DB's country table
            return CountriesSyncAgent.resolve_country(src_value)
        return src_value
