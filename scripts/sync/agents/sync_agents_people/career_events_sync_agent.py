from datetime import date, datetime
import logging
from typing import List

import sqlalchemy as sa
from icms_orm import common
from icms_orm.cmspeople import Person
from icms_orm.common import CareerEvent, CareerEventTypeValues
from sqlalchemy import case
from sqlalchemy.orm.query import Query

from icmsutils.prehistory import SupersedersResolver
from icmsutils.prehistory.prehistory_model import PrehistoryModel
from scripts.mockables import today
from scripts.prescript_setup import db
from scripts.sync.agents import CountriesSyncAgent
from scripts.sync.agents.sync_agents_common import BaseSyncAgent


class CareerEventsSyncAgent(BaseSyncAgent):
    def __init__(self, sync_broker):
        super().__init__(sync_broker)
        self._pre_syncd = dict()
        self._sources:List[Person] = self._broker.relay(self.get_query())
        self._cms_ids = set([p.cmsId for p in self._sources])
        self.__fetch_data()
    
    def __fetch_data(self):
        ce:CareerEvent 
        for ce in CareerEvent.session().query(CareerEvent).all():
            if ce.cms_id in self._cms_ids:
                self._pre_syncd[ce.cms_id] = self._pre_syncd.get(ce.cms_id, list())
                self._pre_syncd[ce.cms_id].append(ce)

    def is_pre_syncd(self, event: CareerEvent) -> bool:
        """
        Checks if existing event (cms_id, date, event_type) is already in the destination DB
        """
        ce: CareerEvent
        for ce in self._pre_syncd.get(event.cms_id, list()):
            if ce.event_type == event.event_type and ce.date == event.date and ce.cms_id == event.cms_id:
                return True
        return False

    def get_query(self) -> Query:
        return Person.session().query(Person)

    def sync(self):
        session = CareerEvent.session()
        possible_events: List[CareerEvent] = list()
        p:Person
        for p in self._sources:
            possible_events.append(CareerEvent.from_ia_dict({
                    CareerEvent.cms_id: p.cmsId,
                    CareerEvent.event_type: CareerEventTypeValues.CMS_DEPARTURE,
                    CareerEvent.date: p.dateDeparture
                }))
            possible_events.append(CareerEvent.from_ia_dict({
                    CareerEvent.cms_id: p.cmsId,
                    CareerEvent.event_type: CareerEventTypeValues.CMS_ARRIVAL,
                    CareerEvent.date: p.dateRegistration.date() if isinstance(p.dateRegistration, datetime) else p.dateRegistration
                }))
        ce: CareerEvent
        for ce in possible_events:
            if ce.date is not None and not self.is_pre_syncd(ce):
                session.add(ce)


