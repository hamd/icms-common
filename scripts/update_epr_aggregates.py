from scripts.prescript_setup import db, config, ConfigProxy
from icms_orm.common import EprAggregate, EprAggregateUnitValues as AggregateUnit, Institute, Person
from icmsutils.businesslogic import servicework as srv
from datetime import date
import logging


class EprAggregatesUpdater(object):

    ISSUE_ZERO_AGG_VALUE = 'zero aggregate value'
    ISSUE_NEGATIVE_CMS_ID = 'negative CMS ID'
    ISSUE_MISSING_CMS_ID = 'missing CMS ID'
    ISSUE_MISSING_INST_CODE = 'missing inst code'

    def __init__(self, cutoff_year=2000):
        self._cutoff_year = cutoff_year
        self._known_inst_codes = {r[0] for r in Institute.session().query(Institute.code).all()}
        self._known_cms_ids = {r[0] for r in Person.session().query(Person.cms_id).all()}
        self._pre_existing = {EprAggregatesUpdater.make_agg_key(r): r for r in
                  EprAggregate.session().query(EprAggregate).filter(EprAggregate.year >= self._cutoff_year).all()}
        self._seen_keys = set()
        self._new_entries = 0
        self._match_hits = 0
        self._match_misses = 0

    @staticmethod
    def make_agg_key(agg):
        assert isinstance(agg, EprAggregate)
        return agg.coordinates

    def _is_new(self, agg):
        if not self.make_agg_key(agg) in self._pre_existing:
            self._new_entries += 1
            return True
        return False

    def _matches_existing(self, agg):
        if -.01 < self._pre_existing[self.make_agg_key(agg)].aggregate_value - agg.aggregate_value < .01:
            self._match_hits += 1
            return True
        self._match_misses += 1
        return False

    def _mark_key_as_seen(self, agg_key):
        self._seen_keys.add(agg_key)

    def run(self):
        logging.info('Will recompute everything from year {0} onwards and check against existing data.'.format(
            self._cutoff_year))
        for _year in range(self._cutoff_year, date.today().year + 1):
            shift_aggs = srv.EprAggregatesFactory.get_shifts_done(year=_year)
            work_aggs = srv.EprAggregatesFactory.get_work_done(year=_year)
            for _col in [shift_aggs, work_aggs]:
                for _el in _col:
                    assert isinstance(_el, EprAggregate)
                    problem = None
                    if not _el.aggregate_value > 0:
                        problem = EprAggregatesUpdater.ISSUE_ZERO_AGG_VALUE
                    elif _el.inst_code not in self._known_inst_codes:
                        problem = EprAggregatesUpdater.ISSUE_MISSING_INST_CODE
                    elif _el.cms_id is not None and _el.cms_id < 0:
                        problem = EprAggregatesUpdater.ISSUE_NEGATIVE_CMS_ID
                    elif _el.cms_id is not None and _el.cms_id not in self._known_cms_ids:
                        problem = EprAggregatesUpdater.ISSUE_MISSING_CMS_ID
                    if problem is not None:
                        _report_fn = logging.warning
                        if problem in {EprAggregatesUpdater.ISSUE_ZERO_AGG_VALUE}:
                            _report_fn = logging.debug
                        _report_fn('Problem with {5} for CMS ID {0} from {4} found with {1} of {2} in {3}!'.
                                        format(_el.cms_id, _el.aggregate_value, _el.aggregate_unit, _el.year,
                                               _el.inst_code, problem))
                    else:
                        # entry is fit for additions, let's see if we have it already
                        if self._is_new(_el):
                            EprAggregate.session().add(_el)
                        else:
                            agg_key = self.make_agg_key(_el)
                            if not self._matches_existing(_el):
                                logging.debug('Updating aggregate for {0} to value: {1}'.format(agg_key, _el.aggregate_value))
                                _predating = self._pre_existing[agg_key]
                                _predating.set(EprAggregate.aggregate_value, _el.aggregate_value)
                                EprAggregate.session().add(_predating)
                            self._mark_key_as_seen(agg_key)

            logging.info('Obtained {0} work aggregates and {1} shift aggregates for year {2}'.format(len(work_aggs), len(shift_aggs), _year))

        # cleaning up any stale pre-existing records that didn't show up this time (source data shrunk?)
        _unseen_pre_existing_keys = self._pre_existing.keys() - self._seen_keys
        for _k in _unseen_pre_existing_keys:
            EprAggregate.session().delete(self._pre_existing[_k])
        EprAggregate.session().commit()

        logging.info('Created {0} new entries, found {1} match hits, {2} match misses and dropped {3} outdated records.'.
            format(self._new_entries, self._match_hits, self._match_misses, len(_unseen_pre_existing_keys)))


if __name__ == '__main__':
    EprAggregatesUpdater(cutoff_year=ConfigProxy.get_epr_aggregates_earliest_update_year()).run()
