from . import db
from icmsutils.funcutils import Cache
from icmsutils.prehistory import PrehistoryModel, PersonAndPrehistoryWriter
from icms_orm.cmspeople import Person, MemberActivity, PersonHistory as History, Institute
from icms_orm.cmspeople import InstStatusValues as ISV, MoData, MemberStatusValues as MSV
import sqlalchemy as sa
from datetime import date
from icmsutils.businesslogic import servicework as swk
from icms_orm.epr import TimeLineUser as TLU


class BlockedEngineerIssue(object):
    def __init__(self, person, act_name, history, mo):
        self._person = person
        self._act_name = act_name
        self._pm = PrehistoryModel(cms_id=person.get(Person.cmsId), person=person, history=history)
        self._mo_data = mo

        self._worked = None
        self._dues = None

    def __str__(self):
        return u'[{cms_id}] {name} {ic}'.format(name=self.name, ic=self.inst_code, cms_id=self.cms_id)

    def __unicode__(self):
        return u'[{cms_id}] {name} {ic}'.format(name=self.name, ic=self.inst_code, cms_id=self.cms_id)

    @property
    def activity(self):
        return self._act_name

    @property
    def inst_code(self):
        return self._person.get(Person.instCode)

    @property
    def cms_id(self):
        return self._person.get(Person.cmsId)

    @property
    def name(self):
        return u'{0} {1}'.format(self._person.get(Person.lastName), self._person.get(Person.firstName))

    @property
    def last_unsuspension(self):
        return self._person.get(Person.dateAuthorUnsuspension)

    @property
    def last_registration(self):
        return self._pm.get_last_effective_arrival_date()

    @property
    def has_any_mo(self):
        yr = date.today().year
        return any([self._mo_data.get(_col) == 'YES' for _col in MoData.get_columns_for_year(yr)])

    @property
    def free_mo(self):
        return self._mo_data.get(MoData.get_free_mo_column_for_year(date.today().year)) == 'YES'

    @property
    def wish_mo(self):
        return self._mo_data.get(MoData.get_mo_column_for_year(date.today().year)) == 'YES'

    @property
    def phd_mo(self):
        return self._mo_data.get(MoData.get_phd_mo_column_for_year(date.today().year)) == 'YES'

    @property
    def nominal_app_start(self):
        return max(self.last_registration, self.last_unsuspension or self.last_registration)

    @property
    def epr_done(self):
        if self._worked is None:
            self._worked = 0
            for _year in range(self.last_registration.year, date.today().year + 1):
                self._worked += swk.get_person_shifts_done(self.cms_id, _year, Person.session())
                self._worked += swk.get_person_worked(self.cms_id, _year, Person.session())
        return self._worked

    @property
    def epr_dues(self):
        if self._dues is None:
            self._dues = 0
            for _year in range(self.nominal_app_start.year, date.today().year + 1):
                self._dues += swk.get_person_due(self.cms_id, _year, Person.session())
        return self._dues

    @property
    def timed_out(self):
        return self.nominal_app_start.year < date.today().year - 2

    @property
    def due_billed(self):
        return self.epr_dues > 0

    @property
    def epr_clear(self):
        return self.epr_done >= 6

    @property
    def can_be_unblocked(self):
        return self._dues > 0 and self._worked >= 6.0
    
    @property
    def verdict(self):
        # _outcome = ''
        # if self.can_be_unblocked:
        #     _outcome += 'unblock '
        # if self.timed_out:
        #     if self.has_any_mo:
        #         _outcome += 'time-out with MO '
        #     _outcome += 'time-out '
        # if self.nominal_app_start.year == 2019 and self.epr_dues == 0:
        #     _outcome += 'assign due '
        # return _outcome or 'unsure'

        _mask = (self.timed_out, self.epr_clear, self.due_billed)

        if _mask in {(True, False, False)}:
            _outcome = 'suspend (time)'
        elif _mask in {(True, False, True)}:
            _outcome = 'suspend (time, due)'
        elif _mask in {(False, True, True)}:
            _outcome = 'unblock (work, due)'
        elif _mask in {(True, True, True)}:
            _outcome = 'unblock (work, due, time)'
        elif _mask in {(False, True, False)}:
            _outcome = 'unblock (work)'
        elif _mask in {(True, True, False)}:
            _outcome = 'unblock (time, work)'
        elif _mask in {(False, False, False)}:
            if self.nominal_app_start.year == date.today().year:
                _outcome='assign due (app starts this year!)'
            else:
                _outcome = 'suspend (no due, no work, no timeout)'
        elif _mask in {(False, False, True)}:
            _outcome = 'let be (due)'

        return _outcome
        # return ' | '.join(_notions)





    def apply_unblocking(self):
        pw = PersonAndPrehistoryWriter(db_session=Person.session(), actor_person=self._person,
                                       object_person=self._person)
        pw.set_new_value(Person.isAuthorBlock, False)
        pw.set_new_value(Person.dateAuthorUnblock, date.today())
        pw.set_new_value(Person.remarksSecr, u'\n'.join([
            '{date}: author-blocked engineers cleanup (unblocking); '.format(date=date.today()),
            self._person.get(Person.remarksSecr) or '']))
        pw.apply_changes(do_commit=True)

    def apply_suspension(self):
        pw = PersonAndPrehistoryWriter(db_session=Person.session(), actor_person=self._person,
                                       object_person=self._person)
        pw.set_new_value(Person.isAuthorSuspended, True)
        pw.set_new_value(Person.dateAuthorSuspension, date.today())
        pw.set_new_value(Person.remarksSecr, u'\n'.join([
            '{date}: author-blocked engineers cleanup (suspending); '.format(date=date.today()),
            self._person.get(Person.remarksSecr) or ''
        ]))
        pw.apply_changes(do_commit=True)

    def apply_dues(self):
        if self.epr_dues > 0:
            raise Exception('That should not be, {cms_id} already has some EPR dues!'.format(cms_id=self.cms_id))
        """just add corerspondng EPR due to the last timeline(s - that's one of the problems) in 2019"""
        if self.nominal_app_start.year < 2019:
            raise Exception('That should not be, {cms_id} would have started their application on {year}'.format(
                cms_id=self.cms_id, year=self.nominal_app_start
            ))
        _tlus = db.session.query(TLU).filter(TLU.cmsId == self.cms_id).filter(TLU.isSuspended == False)\
            .filter(TLU.year == date.today().year).all()
        for _tlu in _tlus:
            assert isinstance(_tlu, TLU)
            # all the work done so far can be considered done prior to the unsuspension so it becomes the X
            _tlu.dueApplicant = _tlu.yearFraction * max(0, 6 - self.epr_done)
            _tlu.comment = u'; '.join([
                '{date}: Applying authorship applicant due of {due}'.format(date=date.today(), due='%.2f' % _tlu.dueApplicant),
                _tlu.comment or '']
            )
            db.session.add(_tlu)
        db.session.commit()

    @classmethod
    def find_issues(cls):
        s = db.session
        q = s.query(Person, MemberActivity.name, History, MoData).\
            join(MemberActivity, MemberActivity.id == Person.activityId).\
            join(History, History.cmsId == Person.cmsId).\
            join(Institute, Institute.code == Person.instCode).\
            join(MoData, MoData.cmsId == Person.cmsId).\
            filter(sa.and_(
                MemberActivity.name.like('%Engineer%'),
                Person.isAuthor == False,
                Person.isAuthorSuspended == False,
                Person.isAuthorBlock == True,
                Institute.cmsStatus == ISV.YES,
                Person.status == MSV.PERSON_STATUS_CMS
        )).order_by(Person.instCode)
        _ans = []
        for _p, _act_name, _history, _mo_data in q.all():
            _ans.append(BlockedEngineerIssue(person=_p, act_name=_act_name, history=_history, mo=_mo_data))
        return _ans

