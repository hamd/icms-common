from scripts.watchdog.units.watchdog_base import AutonomousWatchdogBase, WatchdogRemedy
from icms_orm.common import ApplicationAsset as AppAsset
from scripts.sync.helpers import SyncRunInfo
from scripts.sync.agents import InstitutesSyncAgent, PeopleSyncAgent, AffiliationsSyncAgent
import logging
import datetime

class StuckFwdSyncWatchdog(AutonomousWatchdogBase):

    PATIENCE_HOURS = 4

    def check(self):
        last_run_info = SyncRunInfo.get_last_run_info_from_db()
        most_recent_timestamp = None
        for _class in [InstitutesSyncAgent, PeopleSyncAgent, AffiliationsSyncAgent]:
            _agent_timestamp = last_run_info.get_timestamp(_class)
            most_recent_timestamp = max(most_recent_timestamp or _agent_timestamp, _agent_timestamp)
        logging.debug('Most recent timestamp is %s', most_recent_timestamp)
        dt = datetime.datetime.now() - most_recent_timestamp
        age = dt.days * 24 + dt.seconds / 3600
        if age > self.PATIENCE_HOURS:
            is_busy = last_run_info.is_busy
            busy_str = 'and the busy flag is UP.' if is_busy else 'but the busy flag is DOWN'
            self.add_issue("The most recent sync timestamp is {0:.2f} hours old: {1} {2}".format(age, most_recent_timestamp, busy_str))
            if is_busy:
                self.add_remedy(StuckFwdSyncRemedy())
 


class StuckFwdSyncRemedy(WatchdogRemedy):
    def __init__(self):
        pass

    def apply(self):
        logging.info('Removing forward sync\'s busy flag.')
        last_run_info = SyncRunInfo.get_last_run_info_from_db()
        last_run_info.set_done()
        last_run_info.store_in_db()
