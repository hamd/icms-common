import sqlalchemy as sa
from scripts.prescript_setup import db
from icms_orm.cmspeople import ProjectManagers
from icms_orm.common import Tenure, Position, OrgUnit, OrgUnitType, OrgUnitTypeName, PositionName
from datetime import date
from scripts.watchdog.units.watchdog_base import WatchdogBase, AutonomousWatchdogBase, WatchdogRemedy
import sqlalchemy as sa
import logging


class OldIcmsPMRemedy(WatchdogRemedy):
    def __init__(self, entry_id, cms_id, cms_id_2=0, cms_id_3=0, cms_id_4=0) -> None:
        super().__init__()
        self._entry_id = entry_id
        self._cms_id = cms_id
        self._cms_id_2 = cms_id_2
        self._cms_id_3 = cms_id_3
        self._cms_id_4 = cms_id_4

    def apply(self):
        pm = db.session.query(ProjectManagers).filter(ProjectManagers.id == self._entry_id).one()
        assert isinstance(pm, ProjectManagers)
        pm.cmsId = self._cms_id
        pm.cmsId2 = self._cms_id_2
        pm.cmsId3 = self._cms_id_3
        pm.cmsId4 = self._cms_id_4
        db.session.add(pm)


class OldIcmsProjectManagersWatchdog(AutonomousWatchdogBase):
    def __init__(self):
        super(OldIcmsProjectManagersWatchdog, self).__init__()
        self._year = date.today().year
        self._pm_data = {}
        for row in db.session.query(ProjectManagers).filter(ProjectManagers.year == self._year).all():
            assert isinstance(row, ProjectManagers)
            self._pm_data[row.project_code] = row

    @staticmethod
    def remap_to_tenure_domain_name(project_name):
        return {
            'HGCAL (CE)': 'HGCAL',
            'PPD': 'Physics Performance & Datasets',
            'OFFCOMP': 'Offline & Computing',
            'EC': 'ECAL',
            'HC': 'HCAL',
            'OFF': 'Offline & Computing',
            'CORE': 'Offline & Computing',
            'L1TRG': 'L1 Trigger',
            'TRGCOORD': 'Trigger',
            'TECH': 'Technical',
            'TK': 'TRACKER'
        }.get(project_name, project_name)

    def check(self):
        for code in self._pm_data:
            tenures_data = db.session.query(Tenure, Position, OrgUnit, OrgUnitType).\
                join(Position, Tenure.position_id == Position.id).\
                join(OrgUnit, Tenure.unit_id == OrgUnit.id).\
                join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).\
                filter(OrgUnitType.name.in_([OrgUnitTypeName.SUBDETECTOR, OrgUnitTypeName.COORDINATION_AREA])).\
                filter(OrgUnit.domain.ilike(OldIcmsProjectManagersWatchdog.remap_to_tenure_domain_name(code))).\
                filter(sa.between(date.today(), Tenure.start_date, sa.func.coalesce(Tenure.end_date, date.today()))).\
                filter(sa.or_(Position.name.ilike('%deputy%'), Position.name.in_([PositionName.MANAGER, PositionName.COORDINATOR]))).\
                all()
            if tenures_data is None or len(tenures_data) == 0:
                self.add_issue("Found no tenures for domain {0}".format(code))
            else:
                pm_entry = self._pm_data.get(code)
                assert isinstance(pm_entry, ProjectManagers)
                logging.info("Actually found something for {0}".format(code))
                should_be_managers = set()
                should_be_bosses = set()
                should_be_non_bosses = set()
                current_managers = set(
                    v for v in [pm_entry.cmsId, pm_entry.cmsId2, pm_entry.cmsId3, pm_entry.cmsId4] if v != 0)
                are_bosses = {pm_entry.cmsIdRoot}
                are_non_bosses = {v for v in current_managers if v not in are_bosses}
                for tenure, position, unit, unit_type in tenures_data:
                    assert isinstance(tenure, Tenure)
                    assert isinstance(position, Position)
                    (should_be_bosses if position.level <= 1 else should_be_non_bosses).add(tenure.cms_id)
                    should_be_managers.add(tenure.cms_id)

                if current_managers != should_be_managers:
                    self.add_issue('{0}: should be managers: {1}, are managers: {2}, should be bosses: {3}'.
                                   format(code, should_be_managers, current_managers, should_be_bosses))

                    boss_cms_id = list(should_be_bosses)[0]
                    other_cms_ids = {_id for _id in should_be_managers if _id != boss_cms_id}
                    self.add_remedy(OldIcmsPMRemedy(pm_entry.id, boss_cms_id, *other_cms_ids))
                else:
                    logging.info('{0} LOOKS FINE!'.format(code))

