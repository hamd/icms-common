from scripts.prescript_setup import config, db
from icms_orm.cmspeople import Person, MemberActivity, PersonHistory, Institute
from icms_orm.common import InstituteLeader, Person as NewPerson
from icms_orm.common import EmailMessage
import sqlalchemy as sa
from datetime import date
import logging
import argparse

log: logging.Logger = logging.getLogger(__name__)


class StudentEntry(object):
    def __init__(self, cms_id: int, first_name: str, last_name: str, days: int, inst_code: str, author: bool):
        self.cms_id = cms_id
        self.first_name = first_name
        self.last_name = last_name
        self.days_count = days
        self.inst_code = inst_code
        self.is_author = author

    def __str__(self):
        return f'CMS ID {self.cms_id}, {self.email_string} - PHD Student for {self.days_count // 365} years and {self.days_count % 365} days'

    @property
    def email_string(self):
        return f'  - {self.first_name} {self.last_name} ({not self.is_author and "non-" or ""}author)'


class TeamLeaderEntry(object):
    def __init__(self, first_name, last_name, email_address):
        self.first_name = first_name
        self.last_name = last_name
        self.email_address = email_address

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.email_address})'

    @property
    def email_string(self):
        return f'{self.first_name} {self.last_name}'


class StudentsCleanupAgent(object):

    def __init__(self, sender: str, years_limit: int = 7, member_insts_only: bool = True):
        self._students = []
        self._member_insts_only = member_insts_only
        self._years_limit: int = years_limit
        self._all_entries = list()
        self._entries_by_inst = dict()
        self._team_leaders = dict()
        self._sender: str = sender

    def _find_students(self):
        q = Person.session().query(Person).join(
            MemberActivity, Person.activityId == MemberActivity.id)
        if self._member_insts_only:
            q = q.join(Institute, Person.instCode == Institute.code).filter(
                Institute.cmsStatus.ilike('yes'))
        q = q.filter(MemberActivity.name == 'Doctoral Student').filter(Person.status.like('CMS%')).\
            join(PersonHistory).order_by(Person.instCode, Person.lastName)

        ppl = q.all()

        person: Person
        for person in ppl:
            activity_changes = person.history.get_activity_changes()
            last_change_date = activity_changes[0][1] if activity_changes else None
            days_as_phd = (
                date.today() - (last_change_date or person.dateCreation.date())).days
            if days_as_phd >= self._years_limit * 365:
                entry = StudentEntry(
                    person.cmsId, person.firstName, person.lastName, days_as_phd, person.instCode, person.isAuthor)
                self._all_entries.append(entry)
                self._entries_by_inst[entry.inst_code] = self._entries_by_inst.get(
                    entry.inst_code, list())
                self._entries_by_inst[entry.inst_code].append(entry)

    def _find_team_leaders(self):
        q = NewPerson.session().query(NewPerson.cms_id, NewPerson.email, NewPerson.first_name, NewPerson.last_name, InstituteLeader.inst_code).\
            join(InstituteLeader, InstituteLeader.cms_id == NewPerson.cms_id).\
            filter(InstituteLeader.start_date <= date.today()).\
            filter(sa.or_(InstituteLeader.end_date == None, InstituteLeader.end_date > date.today())).\
            order_by(sa.desc(InstituteLeader.is_primary), InstituteLeader.cms_id, InstituteLeader.inst_code, sa.desc(InstituteLeader.start_date)).\
            distinct(InstituteLeader.is_primary,
                     InstituteLeader.cms_id, InstituteLeader.inst_code)
        for _cms_id, _email, _fname, _lname, _ic in q.all():
            self._team_leaders[_ic] = self._team_leaders.get(_ic, list())
            self._team_leaders[_ic].append(
                TeamLeaderEntry(_fname, _lname, _email))

    def _notify(self):
        _template = """Dear {team_leader},

In CMS, the member database is to be checked for outdated information and cleaned up. In
the course of this review, we ask all institute leaders to check the data of long-term doctoral
students and to correct it if necessary.

The following persons have been registered as CMS doctoral students for **more than {limit} years**:

{list_of_people}

You are listed as CBI for {institute}. Can you tell us about the status of the listed students and whether they are still pursuing a dissertation?

Otherwise, please update the status for the person.

**Please note that the free authorship, typically granted to doctoral students, may be revoked in cases of unusually long studentship.**

The Secretariat can help you with any changes (cms.secretariat@cern.ch).

If the information is correct and the persons are still doctoral students, we ask you to send
us a formal request for the long-term doctoral students; please send it to cms-engagement-office@cern.ch.

If you have any questions, please contact cms-engagement-office@cern.ch.

Yours sincerely,
{sender}
on behalf of Engagement Office
"""

        for _ic in self._entries_by_inst.keys():
            _entries = self._entries_by_inst.get(_ic)
            _leaders = self._team_leaders.get(_ic)
            if not _leaders:
                log.warning('NO TEAM LEADERS FOUND FOR: {0}'.format(_ic))
                continue
            _content = _template.format(
                team_leader=', '.join([_l.email_string for _l in _leaders]),
                limit=self._years_limit,
                list_of_people='\n'.join(
                    [_s.email_string for _s in _entries]),
                institute=_ic,
                sender=self._sender)
            EmailMessage.compose_message(
                sender='cms-engagement-office@cern.ch',
                to=','.join([_l.email_address for _l in _leaders]),
                subject='Students in need of status update', body=_content, cc=None, bcc='icms-support@cern.ch,cms-engagement-office@cern.ch',
                source_app=None, reply_to=None,
                remarks=None, db_session=EmailMessage.session()
            )

    def run(self):
        self._find_students()
        self._find_team_leaders()
        self._notify()


class StudentsCleanupPrintingAgent(StudentsCleanupAgent):
    def _notify(self):
        for _ic in self._entries_by_inst.keys():
            print(f' ==== FOR INSTITUTE {_ic} ====')
            if self._team_leaders.get(_ic):
                print('Team leaders: {0}'.format(
                    str(', '.join([str(l) for l in self._team_leaders.get(_ic)]))))
            else:
                log.warning('NO TEAM LEADERS FOUND FOR: {0}'.format(_ic))
            for _stud in self._entries_by_inst.get(_ic):
                print('- {0}'.format(str(_stud)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Studens cleanup script')
    parser.add_argument('--years', type=int, default=7, help='Student status\' staleness threshold')
    parser.add_argument('--sender', type=str, help='Name and surname to show below the message', required=True)
    parser.add_argument('--all-insts', action='store_true', default=False, help='Forces the search to include non-member institutes too')
    parser.add_argument('--dryrun', action='store_true', default=False, help='Forces reporting findings to console only (no mails)')
    args: argparse.Namespace = parser.parse_args()
    params = dict(sender=args.sender, years_limit=args.years,
                  member_insts_only=not args.all_insts)
    if args.dryrun:
        agent = StudentsCleanupPrintingAgent(**params)
    else:
        agent = StudentsCleanupAgent(**params)
    log.info(f'Launching an instance of {agent.__class__}')
    agent.run()
