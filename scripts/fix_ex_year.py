from icms_orm.common import PrehistoryTimeline

from icmsutils.prehistory import PersonAndPrehistoryWriter, PrehistoryParser, PrehistoryEntry
from scripts.prescript_setup import db
from icms_orm.cmspeople import Person, PersonHistory, MemberStatusValues
import sqlalchemy as sa
import logging
import datetime


class ExYearFixer(object):
    _ex_statuses = [MemberStatusValues.PERSON_STATUS_EXMEMBER, MemberStatusValues.PERSON_STATUS_CMSEXTENDED]

    def __init__(self):
        self._cases = []

    def _find_cases(self):
        q = db.query(Person).joinedload(Person.history).\
            filter(sa.and_(Person.status.in_(ExYearFixer._ex_statuses)),
                   sa.or_(Person.exDate == None, Person.exDate == ''))
        self._cases = q.all()

    def run(self):
        self._find_cases()
        logging.info('Found {0} cases suitable for fixing date end sign'.format(len(self._cases)))
        for p in self._cases:
            try:
                assert isinstance(p, Person)
                assert isinstance(p.history, PersonHistory)

                pp = PrehistoryParser(p.history.history, p)
                exit_date = None
                for line in pp.yield_timelines():
                    assert isinstance(line, PrehistoryTimeline)
                    if line.status_cms in ExYearFixer._ex_statuses:
                        exit_date = line.start_date
                    else:
                        break
                logging.debug('For {0} determined a new departure date: {1}'.format(p.cmsId, exit_date))
                if exit_date is not None and (datetime.date.today() - exit_date).days < 10:
                    papw = PersonAndPrehistoryWriter(db.session, p, p)
                    papw.set_new_value(Person.exDate, exit_date)
                    papw.apply_changes()
                    logging.info('Applied new departure date for {0}: {1}'.format(p.cmsId, exit_date))
                else:
                    logging.debug('Ignoring determined date {0} for {1} as it looks suspicious'.format(p.cmsId, exit_date))
            except Exception as e:
                logging.warning('It did not work for {0}: {1}'.format(p.cmsId, e))
        db.session.commit()


if __name__ == '__main__':
    ExYearFixer().run()
