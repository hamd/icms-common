"""
This script can be thrown away once the one-off MO-sync is performed and if everything matches.
It's not really a test here, rather a check to run on a system before it goes live.
"""
from scripts.prescript_setup import db
from icms_orm.cmspeople import MoData, Institute as OldInst
from icms_orm.common import MO, FundingAgency, MoStatusValues as MoStatus
import sqlalchemy as sa
from icmsutils.businesslogic.superseders import SupersedersResolver
import traceback
import logging


class AbstractMoSyncCheck(object):
    @classmethod
    def check(cls):
        check_value = True
        try:
            for _yr in MoCheckWizard.IMPORT_RANGE:
                check_value = check_value and cls.check_year(_yr)
            return check_value
        except Exception as e:
            logging.debug(traceback.format_exc())
            logging.error(e)
            return False


    @classmethod
    def check_year(cls, year):
        return False


class FundedMoListsCheck(AbstractMoSyncCheck):

    @classmethod
    def check_year(cls, year):
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MoData.get_columns_for_year(year)
        # for everyone?
        rows_old = db.session.query(MoData.cmsId, phd_mo_col, phd_ic_col, phd_fa_col).filter(phd_mo_col.ilike('yes')).all()
        insts_old = {r[0]: SupersedersResolver.get_present_value(OldInst.code, r[-2]) for r in rows_old}
        fas_old = {r[0]: r[-1] for r in rows_old}

        rows_new = db.session.query(MO.cms_id, MO.inst_code, FundingAgency.name).\
            join(FundingAgency, MO.fa_id == FundingAgency.id).filter(
                sa.and_(MO.year == year, MO.status.in_([MoStatus.APPROVED, MoStatus.SWAPPED_OUT])
        )).all()

        insts_new = {r[0]: SupersedersResolver.get_present_value(OldInst.code, r[1]) for r in rows_new}
        fas_new = {r[0]: r[2] for r in rows_new}

        old_only_insts = set(insts_old.values()).difference(set(insts_new.values()))
        new_only_insts = set(insts_new.values()).difference(set(insts_old.values()))

        old_only_fas = set(fas_old.values()).difference(set(fas_new.values()))
        new_only_fas = set(fas_new.values()).difference(set(fas_old.values()))


        assert len(set(insts_old.keys()).intersection(set(insts_new.keys()))) == len(insts_old.keys())
        assert len(set(fas_old.keys()).intersection(set(fas_new.keys()))) == len(fas_old.keys())

        if old_only_insts:
            logging.debug('Insts in old only: {0} [year {1}]'.format(old_only_insts, year))
        if new_only_insts:
            logging.debug('Insts in new only: {0} [year {1}]'.format(new_only_insts, year))
        if old_only_fas:
            logging.debug('FAs in old only: {0} [year {1}]'.format(old_only_fas, year))
        if new_only_fas:
            logging.debug('FAs in new only: {0} [year {1}]'.format(new_only_fas, year))

        assert len(insts_old) == len(insts_new), 'Inst map\'s lengths mismatch: {0} vs {1} [year: {2}]'.\
            format(len(insts_old), len(insts_new), year)
        assert len(fas_old) == len(fas_new), 'FA maps\'s lengths mismatch: {0} vs {1} [year: {2}]'.\
            format(len(fas_old), len(fas_new), year)

        for cms_id, old_ic in insts_old.items():
            assert SupersedersResolver.get_present_value(OldInst.code, old_ic) == insts_new.get(cms_id)

        for cms_id, old_fa in fas_old.items():
                assert old_fa == fas_new.get(cms_id)

        return True


class RequestedMoListsCheck(AbstractMoSyncCheck):
    @classmethod
    def check_year(cls, year):
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MoData.get_columns_for_year(year)

        requested_old = {r[0] for r in db.session.query(MoData.cmsId).filter(sa.and_(
            mo_col.ilike('yes'), phd_mo_col.notilike('yes'), free_mo_col.notilike('yes')
        )).all()}

        requested_new = {r[0] for r in db.session.query(MO.cms_id).filter(sa.and_(
            MO.status == MoStatus.PROPOSED, MO.year == year
        )).all()}

        assert requested_new == requested_old, 'Free MO lists do not match for year {0}'.format(year)
        logging.debug('Found {0} people in requested-mo state for year {1}'.format(len(requested_old), year))
        return True


class FreeMoListsCheck(AbstractMoSyncCheck):
    """
    IMPORTANT: if there is no master_mo for a year, but the free is set - it makes little sense
    - we will sync as confirmed free (granted without request a if). there seem to be only 2 such cases over the years,
      one being apparently a case of converting the extended authorship into free mo
    """
    @classmethod
    def check_year(cls, year):
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MoData.get_columns_for_year(year)

        data_old = {r[0] for r in db.session.query(MoData.cmsId).filter(sa.and_(
            free_mo_col.ilike('yes'), phd_mo_col.notilike('yes')
        )).all()}
        data_new = {r[0] for r in db.session.query(MO.cms_id).filter(sa.and_(
            MO.year == year, MO.status == MoStatus.APPROVED_FREE_GENERIC
        )).all()}

        assert data_new == data_old
        return True


class SwappedOutMoListsCheck(AbstractMoSyncCheck):
    @classmethod
    def check_year(cls, year):
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MoData.get_columns_for_year(year)

        data_old = {r[0] for r in db.session.query(MoData.cmsId).filter(sa.and_(
            mo_col.notilike('yes'), phd_mo_col.ilike('yes')
        )).all()}
        data_new = {r[0] for r in db.session.query(MO.cms_id).filter(sa.and_(
            MO.year == year, MO.status == MoStatus.SWAPPED_OUT
        )).all()}

        assert data_new == data_old
        return True


class FundingAgenciesCheck(AbstractMoSyncCheck):
    @classmethod
    def check(cls):
        src_query = None
        for _yr in MoCheckWizard.IMPORT_RANGE:
            _col = MoData.get_phd_fa_column_for_year(_yr)
            src_query = (lambda q1, q2: q1 and q1.union(q2) or q2)(src_query, db.session.query(_col).distinct())
        old_all = {_r[0] for _r in src_query.distinct().all()}
        new_all = {_r[0] for _r in db.session.query(FundingAgency.name).distinct().all()}
        logging.debug('Found {0} FA names in the old DB vs {1} in the new one'.format(len(old_all), len(new_all)))
        old_only = old_all.difference(new_all)
        if old_only:
            logging.debug('Found only in the old: {0}'.format(old_only))
        new_only = new_all.difference(old_all)
        if new_only:
            logging.debug('Found only in the new: {0}'.format(new_only))

        return len(old_only) == 0 and len(new_only) == 0


class MoCheckWizard(object):

    IMPORT_RANGE = range(2011, 2021)

    @staticmethod
    def main():
        combined_pass = True
        for cls in AbstractMoSyncCheck.__subclasses__():
            result = cls.check()
            logging.info('{0} : {1}'.format(cls.__name__, result and 'passed' or 'FAILED'))
            combined_pass = combined_pass and result
        logging.info('All in all: {0}'.format(combined_pass and 'PASS' or 'FAIL'))
        return combined_pass and 0 or 1



if __name__ == '__main__':
    MoCheckWizard.main()
