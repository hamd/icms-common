#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.send_mails import Message, SMTP
from prescript_setup import db as db

from models import epr, toolkit, common, cmsanalysis as cadi, cmspeople
from sqlalchemy import func
from icmsutils import timedate
from datetime import datetime, date, timedelta
import logging

# a pseudo-config section

ARC_GREEN_STATE = 'ARC-GreenLight'
STALE_STATES = ['PRE-APP', 'PHYS-APP', 'PAS-PUB', 'PUB-Draft', 'CWR-ended', 'FinalReading', 'RefComments']
STALE_DAYS_COUNT = 31
DEAD_DAYS_COUNT = 365

ARC_GREEN_SUBJECT = "[CADI] Please post publication plan for %(code)s."
ARC_GREEN_BODY = """ Dear ARC chair, analysis contact person, and PAG conveners,

  The ARC green light has been set for %(code)s.

  This is a reminder that you should at this time review and then post to the hypernews the publication plan for this analysis.

Best wishes,
   Your CADI system
"""

STALLED_SUBJECT = '[CADI] Please update status for stalled analysis %(code)s.'
STALLED_BODY = """Dear ARC chair and analysis contact person,

  The analysis %(code)s has been stuck in the %(status)s status since %(lastUpdate)s.

Please post a message to the HN summarizing the status of the analysis and/or its review.

Best wishes,
  Your CADI system
"""


def _print_an_hist(
         a # type: cadi.Analysis
        ,h # type: cadi.AnalysisHistory
    ):
    logging.debug('an.code: %s, an.status: %s, hist.field: %s, hist.newVal: %s, hist.date: %s, an.date: %s' %
           (a.code, a.status, h.field, h.newValue, timedate.parse_datetime_verbose(h.updateDate), a.updaterDate))


def get_analysis_start_dates(allowed_states):

    sq = db.session.query(
        func.min(cadi.AnalysisHistory.id).label('historyId'),
        cadi.AnalysisHistory.analysisId.label('analysisId'),
    ).filter(cadi.AnalysisHistory.table == 'Analysis')\
        .group_by(cadi.AnalysisHistory.analysisId).subquery()

    stuff = db.session.query(cadi.AnalysisHistory.updateDate, sq.c.analysisId).\
        join(sq, cadi.AnalysisHistory.id == sq.c.historyId).\
        join(cadi.Analysis, sq.c.analysisId == cadi.Analysis.id).\
        filter(cadi.Analysis.status.in_(STALE_STATES)).\
        all()

    logging.debug("Found %d initial history lines" % len(stuff))

    return {x[1]: timedate.parse_datetime_verbose(x[0]) for x in stuff}


class CadiReminder(object):
    REMINDER_ARC_GREEN = 'ARC_GREEN'
    REMINDER_STALE = 'STALE'

    def __init__(self
                 , analysis  # type: cadi.Analysis
                 , history_entry  # type: cadi.AnalysisHistory
                 , trigger_type  # type: string
                 ):
        self.analysis = analysis
        self.history = history_entry
        self.type = trigger_type

    def get_emails(self):
        emails = []

        contact_user = db.session.query(cmspeople.User). \
            join(cadi.AnalysisAnalysts, cmspeople.User.cmsId == cadi.AnalysisAnalysts.cmsid).filter(cadi.AnalysisAnalysts.analysis == self.analysis.id).one()
        emails.append(contact_user.get_email())

        arc_ppl = db.session.query(cmspeople.User).join(cadi.ARCMember, cmspeople.User.cmsId == cadi.ARCMember.cmsId). \
            join(cadi.ARCsMembersRel, cadi.ARCMember.id == cadi.ARCsMembersRel.memberId). \
            join(cadi.ARC, cadi.ARC.id == cadi.ARCsMembersRel.id).filter(cadi.ARC.id == self.analysis.committee). \
            filter(cadi.ARC.status == 'Accepted').filter(cadi.ARCMember.status == 'Accepted').all()

        for arc_member in arc_ppl:
            emails.append(arc_member.get_email())

        awg_ppl = db.session.query(cmspeople.User).\
            join(cadi.AWGConvener, cadi.AWGConvener.cmsId == cmspeople.User.cmsId).\
            filter(cadi.AWGConvener.awg == self.analysis.awg).filter(cadi.AWGConvener.status == 'Active').all()

        for awg_member in awg_ppl:
            emails.append(awg_member.get_email())

        # logging.debug('Found %d ARC members and %d AWG members' % (len(arc_ppl), len(awg_ppl)))
        return emails

    def get_last_update(self):
        """
        :return: the latest of the two dates stored with Analysis or corresponding History entry
        """
        return max(timedate.parse_date_concise(self.analysis.updaterDate if self.analysis.updaterDate else '1/1/1970'),
                   timedate.parse_datetime_verbose(self.history.updateDate).date())



class CadiRemindersManager(object):

    # subquery to get the last history ENTRY_ID per analysis
    sq_last_hist_id = db.session.query(func.max(cadi.History.id).label('historyId'), cadi.History.masterId.label('analysisId')).\
        filter(cadi.History.table == 'Analysis').group_by(cadi.History.masterId).subquery()

    # subquery to get the first history ENTRY_ID per analysis
    sq_first_hist_id = db.session.query(func.min(cadi.History.id).label('historyId'), cadi.History.masterId.label('analysisId')).\
        filter(cadi.History.table == 'Analysis').group_by(cadi.History.masterId).subquery()

    def __init__(self, exec_date=date.today()):
        self.reminders = []

        self.__find_arc_green_analyses(exec_date)
        self.__find_stale_analyses(exec_date)

    def __find_stale_analyses(self, exec_date):
        # Get all the analyses in one of the stalled states along with their most recent history line
        stalled = db.session.query(cadi.Analysis, cadi.History).\
            join(CadiRemindersManager.sq_last_hist_id, cadi.Analysis.id == CadiRemindersManager.sq_last_hist_id.c.analysisId).\
            join(cadi.History,CadiRemindersManager.sq_last_hist_id.c.historyId == cadi.History.id).\
            filter(cadi.Analysis.status.in_(STALE_STATES)).all()

        logging.debug('Found %d analyses in one of the stalled states' % len(stalled))
        cutoff_date_hi = exec_date - timedelta(days=STALE_DAYS_COUNT)
        cutoff_date_lo = exec_date - timedelta(days=DEAD_DAYS_COUNT)

        for e in stalled:
            cr = CadiReminder(analysis=e[0], history_entry=e[1], trigger_type=CadiReminder.REMINDER_STALE)
            if cutoff_date_lo < cr.get_last_update() < cutoff_date_hi:
                self.reminders.append(cr)

        logging.debug('Found %d analyses last changed before the cutoff date of %s and after the nevermind date of %s' %
                      (len(self.stalled), str(cutoff_date_hi), str(cutoff_date_lo)))

    def __find_arc_green_analyses(self, exec_date):
        date_str = timedate.date_to_str_verbose(exec_date - timedelta(days=1), unpad_day=True) + '%'
        logging.debug('Looking for analysis set to ARC green light on %s' % date_str)
        q = db.session.query(cadi.Analysis, cadi.History).\
            join(cadi.History, cadi.Analysis.id == cadi.History.masterId).filter(cadi.History.table == 'Analysis').\
            filter(cadi.Analysis.status == ARC_GREEN_STATE).filter(cadi.History.newValue == ARC_GREEN_STATE).\
            filter(cadi.History.field == 'status').filter(cadi.History.updateDate.like(date_str))

        for row in q.all():
            self.reminders.append(CadiReminder(analysis=row[0], history_entry=row[1],
                                               trigger_type=CadiReminder.REMINDER_ARC_GREEN))
        logging.debug('Found %d analyses meriting the Green Light reminder' % len(self.arc_greens))

    def send_mails(self):
        for cr in self.reminders:
            msg = Message()
            code = cr.analysis.code[1:] if cr.analysis.code[0] == 'd' else cr.analysis.code
            msg.fromAddress = 'icms-support@cern.ch'
            msg.toAddresses = ['andreas.pfeiffer@cern.ch']
            if cr.type == CadiReminder.REMINDER_STALE:
                msg.subject = STALLED_SUBJECT % {'code': code}
                msg.body = STALLED_BODY % {'code': code}
            elif cr.type == CadiReminder.REMINDER_ARC_GREEN:
                msg.subject = STALLED_SUBJECT % {'code': code}
                msg.body = STALLED_BODY % {'code': code, 'status': cr.analysis.status,
                                           'lastUpdate': cr.get_last_update().isoformat().split('T')[0]}

            if not msg.isValid():
                logging.warn('send_email_async> invalid message found (%s), not sending mail ... ' % str(msg))
                continue
            SMTP().sendEmail(msg.subject, msg.body, msg.fromAddress, msg.toAddresses)

    def print_info(self):
        fmt = '%10s | %10s | %12s | %s'
        print(fmt % ('code', 'type', 'updated', 'emails'))
        print(200*'-')
        for r in self.reminders:
            print(fmt % (r.analysis.code[1:] if r.analysis.code[0] == 'd' else r.analysis.code,
                                        r.type, r.get_last_update(), r.get_emails()))
        print(200 * '-')


    @property
    def arc_greens(self):
        return [x for x in self.reminders if x.type == CadiReminder.REMINDER_ARC_GREEN]

    @property
    def stalled(self):
        return [x for x in self.reminders if x.type == CadiReminder.REMINDER_STALE]

if __name__ == '__main__':
    crm = CadiRemindersManager()
    crm.print_info()
    # crm.send_mails()





