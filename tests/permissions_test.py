from icmsutils.permissions.abstract_classes import AbstractRestrictedResource
from icmsutils.permissions.wrappers import AccessRuleWrapper
from icmsutils.classutils import DictalikeMixin
from typing import List, Set
from icmsutils.permissions import RuleSetChecker, AbstractPermissionsManager, AbstractRestrictedResourcesManager, AbstractAccessRulesProvider
from icmsutils.permissions.rule_checkers import RuleChecker, PredicateChecker
from dataclasses import dataclass, field
from icmsutils.permissions.authorization_context import AuthorizationContext
import pytest


@dataclass
class FakeUser(DictalikeMixin):
    cms_id: int = field(default=42)
    inst_code: str = field(default='CERN')
    subordinates: Set[int] = field(default_factory=set)
    is_root: bool = field(default=False)

    def foo(self):
        return 'bar'

    def foo2(self):
        return f'test-{self.inst_code}'


f = FakeUser(42)
x = FakeUser(43, subordinates=set([42]))
y = FakeUser(54, is_root=True)
z = FakeUser(145, 'DESY')


passing_rule_user_reqs = [
    (dict(), f, dict()),
    ({'u.cms_id': 42}, f, dict()),
    ({'u.cms_id': 'r.cms_id'}, x, dict(cms_id=43)),
    ({'u.cms_id': 'r.cmsId'}, x, dict(cms_id=43)),
    ({'bar': 'u.foo'}, x, dict(token='bar')),
    ({'bar': 'bar'}, x, dict(token='bar')),
    ({'u.foo': 'bar'}, x, dict(token='bar')),
    ({'u.foo': 'r.token'}, x, dict(token='bar')),
    (['u.is_root'], y, dict()),
    ({'u.is_root': True}, y, dict()),
    ({'u.is_root': False}, z, dict()),
    ([True], z, dict()),
    (True, z, dict()),
    ('TRUE', z, dict()),
    ([False, 'true'], z, dict())


]
failing_rule_user_reqs = [
    ({'u.cms_id': 43}, f, dict()),
    ({'u.cms_id': 'r.cms_id'}, x, dict(cms_id=42)),
    ({'u.is_root': False}, y, dict()),
    ({'u.is_root': True}, z, dict()),
    (['false'], z, dict()),
    ('False', z, dict()),
    ('FALSE', z, dict()),
    (False, z, dict()),
    ([], f, dict()),
]


class TestRuleChecking():

    @pytest.mark.parametrize('rule, user, req', passing_rule_user_reqs)
    def test_passing_rules(self, rule, user, req):
        context = AuthorizationContext(dict(u=user, r=req))
        checker = RuleChecker(context=context)
        assert checker.check_rule(rule)

    @pytest.mark.parametrize('rule, user, req', failing_rule_user_reqs)
    def test_failing_rules(self, rule, user, req):
        checker = RuleChecker(AuthorizationContext(dict(u=user, r=req)))
        assert not checker.check_rule(rule)


placeholder_data = [
    (f, dict(cms_id=189), 'u.cms_id', f.cms_id),
    (f, dict(cms_id=189), 'r.cms_id', 189),
    (x, dict(cms_id=189), 'u.foo', 'bar'),
    (z, dict(inst_code='CERN'), 'r.inst_code', 'CERN'),
    (z, dict(instCode='CERN'), 'r.inst_code', 'CERN'),
    (z, dict(inst_code='CERN'), 'r.instCode', 'CERN'),
    (z, dict(inst_code='CERN'), 'u.inst_code', z.inst_code),
    (z, dict(inst_code='CERN'), 'u.foo2', f'test-{z.inst_code}')
]


@pytest.mark.parametrize('user, req, placeholder, value', placeholder_data)
class TestPredicateChecking():
    def test_substitutions(self, user, req, placeholder, value):
        checker = PredicateChecker(AuthorizationContext(dict(u=user, r=req)))
        assert value == checker.substitute_placeholder(placeholder)

    def test_tautologic_predicates(self, user, req, placeholder, value):
        checker = PredicateChecker(AuthorizationContext(dict(u=user, r=req)))
        assert checker.check_predicate(placeholder, placeholder)


passing_ruleset_data = [
    (x, {'cms_id': 42}, [{'u.cms_id': 123}, {'r.cms_id': 'u.subordinates'}]),
    (x, {'cms_id': 42}, []), (x, {'cms_id': 42}, {}),
]
failing_ruleset_data = [
    (z, {'cms_id': 42}, [{'r.cms_id': 'u.subordinates'}]),
]


class TestRuleSetChecking():
    @pytest.mark.parametrize('user, req, ruleset', passing_ruleset_data)
    def test_passing_rulesets(self, user, req, ruleset):
        checker = RuleSetChecker(AuthorizationContext(dict(u=user, r=req)))
        assert checker.check_rule_set(ruleset)

    @pytest.mark.parametrize('user, req, ruleset', failing_ruleset_data)
    def test_failing_rulesets(self, user, req, ruleset):
        checker = RuleSetChecker(AuthorizationContext(dict(u=user, r=req)))
        assert not checker.check_rule_set(ruleset)


"""
What follows is a dummy setup including rudimentary implementations of necessary interfaces
"""


class TestRestrictedResource(AbstractRestrictedResource):
    def __init__(self, id):
        self._id = id

    def _get_id(self):
        return id


class TestResourceManager(AbstractRestrictedResourcesManager):
    @classmethod
    def get_resource_by_key_and_context(cls, key: str, context: AuthorizationContext) -> AbstractRestrictedResource:
        """
        IRL we'd probably look up some DB or a file.
        Here we'll settle for creating a dummy resource for any id we get
        """
        return TestRestrictedResource(id)


class TestRulesProvider(AbstractAccessRulesProvider):
    @classmethod
    def get_rules_for_resource_and_action(cls, resource: AbstractRestrictedResource, action: str) -> List[AccessRuleWrapper]:
        """
        A dummy expecting AuthenticationContext's "r" param store to hold one of the two specified values for "superpowers"
        """
        return [AccessRuleWrapper({'r.superpowers': ['yesplease', 'verymaybe']})]


class TestPermissionsManager(AbstractPermissionsManager):
    @classmethod
    def get_resources_manager_class(cls):
        return TestResourceManager

    @classmethod
    def get_rules_provider_class(cls):
        return TestRulesProvider

    @pytest.mark.parametrize('rule, user, req', passing_rule_user_reqs)
    def test_all_the_failing_rules(self, rule, user, req):
        """
        ! Ignoring the rules - we're simply reusing the samples here 
        """
        context = AuthorizationContext(dict(u=user, r=req))
        assert not TestPermissionsManager.can_perform('write', 42, context)

    @pytest.mark.parametrize('user, req', [(f, {'superpowers': 'thanksbutnothanks'})])
    def test_still_failing_cases(self, user, req):
        context = AuthorizationContext(
            dict(u=user, r=req, x=dict(superpowers='yesplease')))
        assert not TestPermissionsManager.can_perform('write', 42, context)

    @pytest.mark.parametrize('user, req', [(x, {'superpowers': 'yesplease'}), (z, {'superpowers': 'verymaybe'})])
    def test_that_works(self, user, req):
        context = AuthorizationContext(
            dict(u=user, r=req, x=dict(superpowers='NO!')))
        assert TestPermissionsManager.can_perform('write', 42, context)
