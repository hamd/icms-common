from scripts.prescript_setup import db
import pytest
from scripts.sync.helpers import ForwardSyncManager, SyncRunInfo
from scripts.sync.agents import InstitutesSyncAgent, PeopleSyncAgent, AffiliationsSyncAgent, BaseSyncAgent
from icmsutils.icmstest.ddl import recreate_common_db, recreate_people_db
from icms_orm.common import ApplicationAsset
from datetime import datetime
from time import sleep

@pytest.fixture(scope='class')
def setup():
    # we can run the tests here against empty databases
    recreate_people_db()
    recreate_common_db()


def test_sync_run_times_are_stored(setup):

    first_start_time = datetime.now().replace(microsecond=0)
    # the datetime -> timestamp -> datetime conversion roundtrip erases the sub-second part of the datetime.
    sleep(0.5)
    agent_classes = [InstitutesSyncAgent]
    assert db.session.query(ApplicationAsset).count() == 0

    ForwardSyncManager.launch_sync(agent_classes_override=agent_classes)
    first_finish_time = datetime.now().replace(microsecond=0)

    sync_info = SyncRunInfo.get_last_run_info_from_db()

    assert db.session.query(ApplicationAsset).count() == 1
    assert first_start_time <= sync_info.get_timestamp(InstitutesSyncAgent) <= first_finish_time

    sleep(3)
    # FIRST SYNC DONE, NOW LET'S ADD PEOPLE
    agent_classes.append(PeopleSyncAgent)

    second_start_time = datetime.now().replace(microsecond=0)
    sleep(1)
    ForwardSyncManager.launch_sync(agent_classes_override=agent_classes)
    second_finish_time = datetime.now().replace(microsecond=0)
    assert db.session.query(ApplicationAsset).count() == 1
    sync_info = SyncRunInfo.get_last_run_info_from_db()
    assert second_start_time <= sync_info.get_timestamp(InstitutesSyncAgent) <= second_finish_time
    assert second_start_time <= sync_info.get_timestamp(PeopleSyncAgent) <= second_finish_time






class TestBusyFlagManipulation():
    _last_midsync_run_info = None

    @classmethod
    def fetch_last_run_info(cls):
        cls._last_midsync_run_info = SyncRunInfo.get_last_run_info_from_db()

    @classmethod
    def pop_last_fetched_info(cls):
        val = cls._last_midsync_run_info
        cls._last_midsync_run_info = None
        return val

    class CustomSyncAgent(BaseSyncAgent):
        def sync(self):
            TestBusyFlagManipulation.fetch_last_run_info()

    class SimpleFailingSyncAgent(CustomSyncAgent):
        def sync(self):
            super().sync()
            raise ValueError("Imagine that something goes terribly wrong")

    class IntegrityViolatingSyncAgent(CustomSyncAgent):
        def sync(self):
            super().sync()
            from icms_orm.common import Affiliation
            Affiliation.session().add(Affiliation.from_ia_dict({
                Affiliation.cms_id: -999,
                Affiliation.inst_code: 'XYZ',
            }))
            Affiliation.session().commit()

    @pytest.mark.parametrize('agent_class', [CustomSyncAgent, SimpleFailingSyncAgent, IntegrityViolatingSyncAgent])
    def test_busy_flag_behavior(self, setup, agent_class):
        presync_info = SyncRunInfo.get_last_run_info_from_db()
        assert presync_info is None or presync_info.is_busy is not True, "Cannot start with the busy flag set"
        assert issubclass(agent_class, TestBusyFlagManipulation.CustomSyncAgent)
        assert TestBusyFlagManipulation.pop_last_fetched_info() is None
        ForwardSyncManager.launch_sync(agent_classes_override=[agent_class])
        midsync_info = TestBusyFlagManipulation.pop_last_fetched_info()
        assert midsync_info is not None
        assert midsync_info.is_busy is True, 'With a sync in progress the busy flag was supposed to be set!'
        sync_info = SyncRunInfo.get_last_run_info_from_db()
        assert sync_info.is_busy is False, "Despite the previous sync\'s failure, the busy flag should have been lifted"
