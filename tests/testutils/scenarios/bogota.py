from .scenarios import TestScenario
from icms_orm.epr import Task, Pledge, EprInstitute as EprInst, EprUser
from icms_orm.cmspeople import Person, Institute
from icmsutils.icmstest.mock import icms_inst, icms_user, MockEprTaskFactory, make_mock_pledge
from datetime import date

from icmsutils.icmstest.mock import doc, stu, setup_capacities, MockInstituteFactory


class BogotaScenario(TestScenario):

    def run(self):
        db = self.db

        setup_capacities(db)
        
        epr_task_2015 = MockEprTaskFactory.create_instance(instance_overrides={Task.code: 'MOCK_TASK_15', Task.year: 2015, Task.tType: 'EasyWork'})
        epr_task_2016 = MockEprTaskFactory.create_instance(instance_overrides={Task.code: 'MOCK_TASK_16', Task.year: 2016, Task.tType: 'HardWork'})

        db.session.add(epr_task_2015)
        db.session.add(epr_task_2016)

        db.session.add(icms_inst('BOGOTA', 'Yes', 'Bogota', 'Colombia', date(2010, 1, 1)))
        db.session.flush()

        for x in icms_user(1, 1, 'Jesus', 'Gabriel', 'CMS', 'BOGOTA', stu, True, False, 'gabrielj', date(2015, 1, 1)):
            db.session.add(x)
        db.session.commit()
        print('Jesus will do some work both as suspended student in 2015 and unsuspended student in 2016.')
        yield date(2015, 1, 1)

        print('BOGOTA and Jesus Gabriel should now be synced to EPR')
        bogota_epr = db.session.query(EprInst).filter(EprInst.code == 'BOGOTA').one()
        jesus_epr = db.session.query(EprUser).filter(EprUser.name.like('%Gabriel%')).one()

        db.session.add(make_mock_pledge({
            Pledge.code: 'mock_2015_1', Pledge.instId: bogota_epr.id, Pledge.userId: jesus_epr.id, Pledge.workTimeDone: 2,
            Pledge.workedSoFar: 2, Pledge.status: 'DONE', Pledge.taskId: epr_task_2015.id, Pledge.year: 2015,
            Pledge.workTimeAcc: 2, Pledge.workTimePld: 2
        }))

        db.session.commit()
        yield date(2015, 12, 31)

        ### END OF 2015

        for x in icms_user(2, 2, 'Jorge', 'Martinez', 'CMS', 'BOGOTA', stu, False, False, 'martinez', date(2015, 1, 1)):
            db.session.add(x)
        db.session.commit()
        print('2016 starts, there is only Jorge Martinez in BOGOTA')
        yield date(2016, 1, 1)

        for x in icms_user(3, 3, 'Oscar', 'Sanchez', 'CMS', 'BOGOTA', stu, False, False, 'sanchezo', date(2016, 3, 17)):
            db.session.add(x)
        jesus = db.query(Person).filter(Person.lastName == 'Gabriel').one()
        setattr(jesus, Person.isAuthorSuspended.key, False)
        setattr(jesus, Person.dateAuthorUnsuspension.key, date(2016, 3, 17))
        db.session.add(jesus)
        db.session.commit()

        print('Oscar Sanchez joins BOGOTA as a NonDoc student. He will do 3.5 months of EPR work this year.')
        print('Jesus Gabriel becomes unsuspended but he remains a student')
        yield date(2016, 3, 18)

        oscar_epr = db.session.query(EprUser).filter(EprUser.name.like('%Sanchez%')).one()
        jorge_epr = db.session.query(EprUser).filter(EprUser.name.like('%Martinez%')).one()

        db.session.add(make_mock_pledge({
            Pledge.code: 'mock_2016_1', Pledge.instId: bogota_epr.id, Pledge.userId: oscar_epr.id, Pledge.workTimeDone: 3.5,
            Pledge.workedSoFar: 3.5, Pledge.status: 'DONE', Pledge.taskId: epr_task_2016.id, Pledge.year: 2016,
            Pledge.workTimeAcc: 3.5, Pledge.workTimePld: 3.5
        }))

        db.session.add(make_mock_pledge({
            Pledge.code: 'mock_2016_2', Pledge.instId: bogota_epr.id, Pledge.userId: jorge_epr.id, Pledge.workTimeDone: 6.5,
            Pledge.workedSoFar: 6.5, Pledge.status: 'DONE', Pledge.taskId: epr_task_2016.id, Pledge.year: 2016,
            Pledge.workTimeAcc: 6.5, Pledge.workTimePld: 6.5
        }))

        db.session.add(make_mock_pledge({
            Pledge.code: 'mock_2016_3', Pledge.instId: bogota_epr.id, Pledge.userId: jesus_epr.id, Pledge.workTimeDone: 4,
            Pledge.workedSoFar: 4, Pledge.status: 'DONE', Pledge.taskId: epr_task_2016.id, Pledge.year: 2016,
            Pledge.workTimeAcc: 4, Pledge.workTimePld: 4
        }))

        db.session.commit()
        print('BOGOTA should have 3.5 months worked by Oscar plus 6.5 by Jorge on their account but no EPR due in 2016')
        yield date(2016, 12, 31)

        ### END OF 2016

        print('Nothing changes at the beginning of 2017.')
        yield date(2017, 1, 1)

        oscar = db.session.query(Person).filter(Person.lastName == 'Sanchez').one()
        setattr(oscar, Person.activityId.key, doc)
        db.session.add(oscar)
        setattr(jesus, Person.activityId.key, doc)
        db.session.add(jesus)
        db.session.commit()
        print('But on 01.04.2017 Oscar becomes a Doctoral Student and he starts his authorship application.')
        print('Oscar\'s due would be 4.5 (6*0.75) at this point but his target should now be 6-3.5=2.5, '
              'so his 2017 due should be 2.5 * 0.75 = 1.875')
        print('Meanwhile Jorge\'s due should be 0 since he has already worked more than 6 months in 2016.')
        print('In the same time Jesus becomes a Doctoral Student, his application starts!')
        yield date(2017, 4, 1)

        ### END OF 2017

        print('In 2018 Oscar\'s due should be the complement of 6-X where X=3.5. We should then get 2.5-1.875=0.625')
        print('Jorge\'s due should remain 0')
        yield date(2018, 1, 1)


class BogotaScenarioWithFreeloaders(BogotaScenario):
    def run(self):
        db = self.db
        prev_day = None
        # for day in BogotaScenario.run(self):
        for day in super(BogotaScenarioWithFreeloaders, self).run():
            if prev_day and prev_day < date(2016, 4, 1) < day:
                for x in icms_user(13, 13, 'Matheo', 'Pereira', 'CMS', 'BOGOTA', doc, False, False, 'pereiram',
                                   date(2015, 7, 1)):
                    db.session.add(x)
                db.session.commit()
                print('BOGOTA will start with an extra freeloader student this time.')
                print('This will be the first time the script sees him despite his data suggesting '
                      'he\'s been like this since 2015.07.01.')
                yield date(2016, 4, 1)
            yield day
            prev_day = day
