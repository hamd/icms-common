SQLAlchemy==1.3.23
SQLAlchemy-Continuum==1.3.9
psycopg2==2.8.6
pycountry==20.7.3
python-ldap==3.2.0
pytz==2019.1
dataclasses==0.7
