from scripts.prescript_setup import db
from icmsutils.businesslogic.authorship.queries import determine_authorship_application_start_date
from icms_orm.cmspeople import Person
import traceback


def main():
    exceptions_map = dict()
    troublesome_cms_ids = dict()

    all_cms_ids = db.session.query(Person.cmsId).all()

    for i, (cms_id,) in enumerate(all_cms_ids):
        if i % 100 == 0:
            print('Progress %.2f%%' % (100.0 * i / len(all_cms_ids)))
        try:
            msg = 'Determined authorship application start for %d is %s' % (
            cms_id, determine_authorship_application_start_date(cms_id=cms_id, reraise=True))

        except Exception as e:
            exc_class = e.__class__
            exceptions_map[exc_class] = exceptions_map.get(exc_class, [])
            exceptions_map[exc_class].append(e)
            troublesome_cms_ids[exc_class] = troublesome_cms_ids.get(exc_class, [])
            troublesome_cms_ids[exc_class].append(cms_id)
            traceback.print_exc()
            print(e)

    print('Caught %d exceptions' % sum(len(l) for l in exceptions_map.values()))
    print('Encountered %d distinct types of problems' % len(exceptions_map.values()))
    for exc_class, cms_ids in troublesome_cms_ids.items():
        print('%3d issues of type %s: %s' % (len(cms_ids), exc_class.__name__, cms_ids))


if __name__ == '__main__':
    main()
