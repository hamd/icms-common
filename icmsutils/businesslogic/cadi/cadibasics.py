import json, datetime
from icmsutils.funcutils import Cache
import logging
from icms_orm.cmsanalysis import AWG, AWGConvener, Note as CadiNote, AnalysisNoteRel as CadiNoteRel, Analysis, History
from icms_orm.cmspeople import Person
import re, sqlalchemy as sa
from urllib import request


class CadiState(object):

    FREE = None
    PLANNED = None
    # ...
    PRE_APP = None
    # ...
    PHYS_APP = None
    # ...
    CWR = None
    # ...
    SUB = None
    # ...
    ACCEPT = None


    def __init__(self, code, desc):
        self.code = code
        self.desc = desc

    @staticmethod
    def get_ordered_state_names():
        return ['Free', 'Planned', 'Started', 'AWG', 'GoingToPreApp', 'PRE-APP', 'ARC-GreenLight', 'PHYS-APP',
                'PAS-readyForPUB', 'PAS-PUB', 'PAS-only-PUB', 'PUB-Draft', 'ReadyForCWR', 'CWR', 'CWR-ended',
                'CWR/RESP',
                'CWR-d', 'ReadyForFR', 'FinalReading', 'ReadyForSub', 'SUB', 'RefComments', 'ReSubmitted', 'ACCEPT',
                'PUB', 'SUPERSEDED', 'Waiting', 'Thesis-Approved', 'Inactive', 'Completed']


CadiState.FREE = CadiState('Free', 'Free')
CadiState.PLANNED = CadiState('Planned', 'Planned')
CadiState.PRE_APP = CadiState('PRE-APP', 'Pre-Approval')
CadiState.PHYS_APP = CadiState('PHYS-APP', 'Physics Approval')
CadiState.CWR = CadiState('CWR', 'Collaboration-Wide Review')
CadiState.SUB = CadiState('SUB', 'Submitted to Journal')
CadiState.ACCEPT = CadiState('ACCEPT', 'Accepted by Journal')


def get_cds_papers():
    jrec = 1
    rg = 200
    all_stuff = []

    while True:
        url = 'https://cds.cern.ch/search?ln=en&ot=report_number,primary_report_number,recid&as=1&m1=a&p1=CMS&' \
              'f1=reportnumber&op1=a&action_search=Search&c=CMS+Papers&c=&sf=&so=a&rm=&rg=%d&sc=1&of=recjson&' \
              'jrec=%d' % (rg, jrec)
        stuff = json.loads(request.urlopen(url).read())
        all_stuff += stuff
        logging.debug('Got %d records this loop' % len(stuff))
        jrec += len(stuff)
        if len(stuff) < rg:
            break

    data = dict()
    for record in all_stuff:
        # The AN code sometimes shows iup in primary_report_number, sometimes in report_number, sometimes in a list,
        # sometimes in a list of dicts or a oin a dict, always prepended with CMS-, sometimes appended with version number
        # and so on... going for a regex search, leaving the initial code as comment

        record_id = record.get('recid')
        m = re.search(r'CMS-((:?[^-]+)-(:?[\d]+)-(:?[\d]+))', str(record))
        data[record_id] = m and m.groups()[0] or None

    return data


def get_awg_from_analysis_code(code):
    m = re.search(r'([A-Z][^-]+)', code)
    if m:
        return m.group(0)
    return None


def get_awg_members(ssn, awg_name, cols=None):
    cols = cols or [Person.cmsId]
    rows = ssn.query(*cols).join(AWGConvener, AWGConvener.cmsId == Person.cmsId).join(AWG, AWG.id == AWGConvener.awg) \
        .filter(AWG.name == awg_name).all()
    if len(cols) == 1:
        return {getattr(row, cols[0].key) for row in rows}
    else:
        # return [{col.key: getattr(row, col.key) for col in cols} for row in rows]
        return rows


def get_related_cms_note_ids(ssn, line_code):
    rows = ssn.query(CadiNote.cmsNoteId).join(CadiNoteRel, CadiNoteRel.noteId == CadiNote.id). \
        join(Analysis, Analysis.id == CadiNoteRel.id). \
        filter(sa.or_(Analysis.code == line_code, Analysis.code == ('d%s' % line_code))).order_by(CadiNote.cmsNoteId)\
        .all()
    return [r[0] for r in rows]




