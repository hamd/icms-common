from icmsutils.businesslogic.mo.entities import MoState, MoEntry
from icmsutils.businesslogic.people import CmsUser
from icms_orm.common import MoStatusValues as MoStatusValues, Person, MO
from icmsutils.businesslogic.mo.exceptions import MoException
import sqlalchemy as sa
from datetime import date


class MoStatusManager(object):
    __space = [MoState(_name) for _name in MoStatusValues.values()]

    @classmethod
    def is_mo_editing_open(cls, year: int):
        return False

    @classmethod
    def set_new_state(cls, subject: CmsUser, year: int, next_state: MoState, actor_cms_id: CmsUser):
        if year < date.today().year:
            raise MoException('Cannot set new MO status for year {0}'.format(year))
        most_recent_entry = MoStatusManager.get_present_entry(year=year, subject=subject)
        if most_recent_entry is None:
            if next_state not in MoState.get_possible_initial_states():
                raise MoException('Illegal initial state "{0}" requested!'.format(next_state.name))
        else:
            most_recent_state = most_recent_entry.state if most_recent_entry else None
            if not most_recent_state.can_be_followed_by(next_state):
                raise ('State {0} cannot follow the curent state {1}!'.format(next_state.name, most_recent_state.name))
        # not fully out of the woods yet - need to check some permissions perhaps (or not, depending on the conception)


    @classmethod
    def get_history(cls, subject: CmsUser, year: int) -> [MoState]:
        ssn = MO.session()
        q = ssn.query(MO).filter(sa.and_(MO.year == year, MO.cms_id == subject.cms_id)).order_by(sa.desc(MO.timestamp))
        return [MoEntry.from_dao(mo) for mo in q.all()]

    @classmethod
    def get_present_entries(cls, year: int):
        ssn = MO.session()
        q = ssn.query(MO).filter(MO.year == year).order_by(MO.cms_id, sa.desc(MO.timestamp)).distinct(MO.cms_id)
        return [MoEntry(dao) for dao in q.all()]

    @classmethod
    def get_present_entry(cls, year: int, subject: CmsUser):
        q = MO.session().query(MO).filter(sa.and_(MO.cms_id == subject.cms_id, MO.year == year)).\
            order_by(sa.desc(MO.timestamp))
        return MoEntry.from_dao(q.first())



