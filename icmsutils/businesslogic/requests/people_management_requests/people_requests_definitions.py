from typing import Type
from icmsutils.businesslogic.requests import BaseRequestDefinition
from icmsutils.businesslogic.requests.people_management_requests import PersonStatusChangeExecutor, PersonStatusChangeProcessor


class PersonStatusChangeRequestDefinition(BaseRequestDefinition):
    @classmethod
    def get_name(cls) -> str:
        return 'CHANGE_PERSON_STATUS'

    @classmethod
    def get_executor_class(cls) -> Type[PersonStatusChangeExecutor]:
        return PersonStatusChangeExecutor

    @classmethod
    def get_processor_class(cls) -> Type[PersonStatusChangeProcessor]:
        return PersonStatusChangeProcessor

# class PersonActivityChangeRequestDefinition(BaseRequestDefinition):
#     @classmethod
#     def get_name(cls) -> str:
#         return 'CHANGE_PERSON_ACTIVITY'

#     @classmethod
#     def get_executor_class(cls) -> Type[PersonStatusChangeExecutor]:
#         return PersonStatusChangeExecutor

#     @classmethod
#     def get_processor_class(cls) -> Type[PersonStatusChangeProcessor]:
#         return PersonStatusChangeProcessor



    