from icmsutils.businesslogic.requests.base_classes import BaseRequestExecutor
from icmsutils.businesslogic.requests.people_management_requests.person_status_request import PersonStatusChangeProcessor


class PersonMoChangeProcessor(PersonStatusChangeProcessor):
    pass


class PersonMoChangeExecutor(BaseRequestExecutor):
    def __init__(self, cms_id: int, actor_cms_id: int, year: int, mo_status: str):
        self._cms_id = cms_id
        self._actor_cms_id = actor_cms_id
        self._year = year
        self._mo_status = mo_status

    def execute(self):
        return super().execute()
