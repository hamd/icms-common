from typing import Set
import stringcase


class AuthorizationContext():
    """
    Used to hold the parameter stores and resolve placeholders
    """

    @classmethod
    def get_placeholder_separator(cls):
        return "."

    def __init__(self, stores_dict=None):
        self._stores = (stores_dict is not None and stores_dict or dict())

    def register_parameter_store(self, placeholder_prefix: str, store: dict()):
        self._stores[placeholder_prefix] = (store if store is not None else dict())

    def get_placeholder_prefixes(self) -> Set[str]:
        return set(self._stores.keys())

    def resolve_placeholder_value(self, placeholder: str):
        if placeholder.strip().lower() == 'true':
            return True
        if placeholder.strip().lower() == 'false':
            return False
        tokens = placeholder.split(self.get_placeholder_separator())
        if len(tokens) != 2:
            raise AttributeError(
                f'{placeholder} does not contain a valid placeholder separator: «{self.get_placeholder_separator()}»')
        store_name, prop_name = tokens
        if store_name not in self.get_placeholder_prefixes():
            raise AttributeError(
                f'{placeholder} is not a valid placeholder. Supported prefixes: {self.get_placeholder_prefixes()}')
        store = self._stores.get(store_name)
        prop_name = self._pick_case(prop_name, store)
        result = store.get(prop_name)
        if callable(result):
            return result()
        return result

    def is_supported_placeholder(self, placeholder: str):
        if placeholder.lower() in ['false', 'true']:
            return True
        if placeholder.count('.') == 1 and placeholder.split('.')[0] in self.get_placeholder_prefixes():
            return True
        return False

    def _pick_case(self, key, store):
        """
        This one is a bit sketchy as it might allow smuggling request variables to beat the rules checking.
        """
        for case_method in [lambda x: x, stringcase.camelcase, stringcase.snakecase]:
            if case_method(key) in store:
                return case_method(key)
        return key
