from icmsutils.permissions.authorization_context import AuthorizationContext
import logging
from collections import Iterable
from six import string_types


class RuleSetChecker():
    def __init__(self, context: AuthorizationContext):
        self._context = context

    def check_rule_set(self, rule_set):
        rule_checker = RuleChecker(self._context)
        if len(rule_set) == 0:
            return True
        for _rule in rule_set:
            if rule_checker.check_rule(_rule):
                return True
        return False


class RuleChecker():
    def __init__(self, context: AuthorizationContext):
        self._context = context
        self._predicate_checker = PredicateChecker(self._context)

    def check_rule(self, rule) -> bool:
        if isinstance(rule, list):
            # a list of dicts means a list of alternative rules. Special case [] => False
            for _sub_rule in rule:
                if self.check_rule(_sub_rule):
                    return True
        elif isinstance(rule, dict):
            # any contraDiction => False, empty dict => True (no contradictions)
            if len(rule) == 0:
                logging.debug('Encountered an empty set of constraints: %s - nothing will be filtered out.', rule)
                return True
            for predicate_left, predicate_right in rule.items():
                if not self._predicate_checker.check_predicate(predicate_left, predicate_right):
                    # logging.debug('CMS ID %s and request params %s do not fulfill the requirements of rule %s: %s',
                    #               self._user.cms_id, self._request_params, predicate_left, predicate_right)
                    return False
            # logging.debug('Rule %s met for CMS ID %s', rule, self._user.cms_id)
            return True
        elif isinstance(rule, bool):
            return rule
        return self._predicate_checker.check_predicate(rule, True)


class PredicateChecker():
    def __init__(self, context:AuthorizationContext):
        self._context = context

    def substitute_placeholder(self, possible_placeholder):
        if not isinstance(possible_placeholder, string_types) or not self._context.is_supported_placeholder(placeholder=possible_placeholder):
            # placeholders can only be strings
            return possible_placeholder
        return self._context.resolve_placeholder_value(possible_placeholder)
    

    @staticmethod
    def is_collection(value):
        return value is not None and isinstance(value, Iterable) and not isinstance(value, string_types)

    def check_predicate(self, parameter, expected_value):
        left_value = self.substitute_placeholder(parameter)
        right_value = self.substitute_placeholder(expected_value)

        for left_pick in (left_value if PredicateChecker.is_collection(left_value) else [left_value]):
            for right_pick in (right_value if PredicateChecker.is_collection(right_value) else [right_value]):
                # beware of 1 not equaling '1'
                if str(left_pick) == str(right_pick):
                    return True
        return False
