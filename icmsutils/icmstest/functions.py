def cartesian(a, b):
    (a, b) = (list(_e() if callable(_e) else _e) for _e in (a, b))
    return zip(sorted(a * len(b)), b * len(a))